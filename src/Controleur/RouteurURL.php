<?php
namespace TheFeed\Controleur;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use TheFeed\Controleur\ControleurUtilisateur;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\Conteneur;
use TheFeed\Lib\MessageFlash;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;



class RouteurURL
{
    public static function traiterRequete() {
        $requete = Request::createFromGlobals();

        $pathInfo = $requete->getPathInfo();

        $routes = new RouteCollection();

        // Route afficherListe
        $route = new Route("/publications", [
            "_controller" => "\TheFeed\Controleur\ControleurPublication::afficherListe",
        ]);
        $route->setMethods(["GET"]);
        $routes->add("afficherListe", $route);

        // Route afficherFormulaireConnexion
        $route = new Route("/connexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherFormulaireConnexion",
        ]);
        $route->setMethods(["GET"]);
        $routes->add("afficherFormulaireConnexion", $route);

        // Route connecter
        $route = new Route("/connexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::connecter",
        ]);
        $route->setMethods(["POST"]);
        $routes->add("connecter", $route);

        // Route /web
        $route = new Route("/", [
            "_controller" => "\TheFeed\Controleur\ControleurPublication::afficherListe",
        ]);
        $routes->add("afficherListeWeb", $route);

        // Route deconnexion
        $route = new Route("/deconnexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::deconnecter",
        ]);
        $route->setMethods(["GET"]);
        $routes->add("deconnexion", $route);

        // Route publications
        $route = new Route("/publications", [
            "_controller" => "\TheFeed\Controleur\ControleurPublication::creerDepuisFormulaire",
        ]);
        $route->setMethods(["POST"]);
        $routes->add("publications", $route);

        // Route inscriptionGET
        $route = new Route("/inscription", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherFormulaireCreation",
        ]);
        $route->setMethods(["GET"]);
        $routes->add("inscriptionGET", $route);

        // Route inscriptionPOST
        $route = new Route("/inscription", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::creerDepuisFormulaire",
        ]);
        $route->setMethods(["POST"]);
        $routes->add("inscriptionPOST", $route);

        // Route afficherPublications
        $route = new Route("/utilisateurs/{idUtilisateur}/publications", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherPublications",
        ]);
        $route->setMethods(["GET"]);
        $routes->add("afficherPublications", $route);

        $contexteRequete = (new RequestContext())->fromRequest($requete);

        $generateurUrl = new UrlGenerator($routes, $contexteRequete);
        $assistantUrl = new UrlHelper(new RequestStack(), $contexteRequete);

        $twigLoader = new FilesystemLoader(__DIR__ . '/../vue/');
        $twig = new Environment(
            $twigLoader,
            [
                'autoescape' => 'html',
                'strict_variables' => true
            ]
        );
        Conteneur::ajouterService("twig", $twig);

        Conteneur::ajouterService("generateurUrl", $generateurUrl);
        Conteneur::ajouterService("assistantUrl", $assistantUrl);

        $twig->addFunction(new TwigFunction('route', function ($route, $parametres = []) use ($generateurUrl) {
            return $generateurUrl->generate($route, $parametres);
        }));
        $twig->addFunction(new TwigFunction('asset', function ($route, $parametres = []) use ($assistantUrl) {
            return $assistantUrl->getAbsoluteUrl($route, $parametres);
        }));

        $twig->addGlobal('idUtilisateurConnecte', ConnexionUtilisateur::getIdUtilisateurConnecte());
        $twig->addGlobal('messageFlash', new MessageFlash());

        try {
            $associateurUrl = new UrlMatcher($routes, $contexteRequete);
            $donneesRoute = $associateurUrl->match($requete->getPathInfo());
            $requete->attributes->add($donneesRoute);

            $resolveurDeControleur = new ControllerResolver();
            $controleur = $resolveurDeControleur->getController($requete);

            $resolveurDArguments = new ArgumentResolver();
            $arguments = $resolveurDArguments->getArguments($requete, $controleur);

            $reponse = call_user_func_array($controleur, $arguments);
        } catch (ResourceNotFoundException $exception) {
            // Remplacez xxx par le bon code d'erreur
            $reponse = ControleurGenerique::afficherErreur($exception->getMessage(), 404);
        } catch (MethodNotAllowedException $exception) {
            // Remplacez xxx par le bon code d'erreur
            $reponse = ControleurGenerique::afficherErreur($exception->getMessage(), 405);
        } catch (\Exception $exception) {
            $reponse = ControleurGenerique::afficherErreur($exception->getMessage(), 400);
        }
        $reponse->send();


    }
}