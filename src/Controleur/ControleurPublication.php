<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;

class ControleurPublication extends ControleurGenerique
{

    public static function afficherListe(): Response
    {
        $publications = (new PublicationRepository())->recuperer();
        return ControleurUtilisateur::afficherTwig('publication/feed.html.twig', [
            "publications" => $publications
        ]);
    }

    public static function creerDepuisFormulaire(): RedirectResponse
    {
        $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($idUtilisateurConnecte);

        if ($utilisateur == null) {
            MessageFlash::ajouter("error", "Il faut être connecté pour publier un feed");
            return ControleurPublication::rediriger('connexion');
        }
        
        $message = $_POST['message'];
        if ($message == null || $message == "") {
            MessageFlash::ajouter("error", "Le message ne peut pas être vide!");
            return ControleurPublication::rediriger('publications', (array)'afficherListe');
        }
        if (strlen($message) > 250) {
            MessageFlash::ajouter("error", "Le message ne peut pas dépasser 250 caractères!");
            return ControleurPublication::rediriger('publications', (array)'afficherListe');
        }

        $publication = Publication::create($message, $utilisateur);
        (new PublicationRepository())->ajouter($publication);

        return ControleurPublication::rediriger('publications', (array)'afficherListe');
    }


}