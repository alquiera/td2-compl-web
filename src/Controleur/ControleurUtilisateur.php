<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Configuration\Configuration;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur extends ControleurGenerique
{

    public static function afficherErreur($messageErreur = "", $controleur = ""): ?Response
    {
        parent::afficherErreur($messageErreur, "utilisateur");
        return null;
    }

    public static function afficherPublications($idUtilisateur): ?Response
    {

            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($idUtilisateur);
            if ($utilisateur === null) {
                MessageFlash::ajouter("error", "Login inconnu.");
                return ControleurUtilisateur::rediriger("publications", (array)"afficherListe");
            } else {
                $loginHTML = htmlspecialchars($utilisateur->getLogin());
                $publications = (new PublicationRepository())->recupererParAuteur($idUtilisateur);
                return ControleurUtilisateur::afficherTwig('publication/feed.html.twig', [
                    "publications" => $publications,
                    "login" => $loginHTML
                ]);
            }
    }

    public static function afficherFormulaireCreation(): Response
    {
        return ControleurUtilisateur::afficherTwig('utilisateur/inscription.html.twig');
    }

    public static function creerDepuisFormulaire(): RedirectResponse
    {
        if (
            isset($_POST['login']) && isset($_POST['mot-de-passe']) && isset($_POST['email'])
            && isset($_FILES['nom-photo-de-profil'])
        ) {
            $login = $_POST['login'];
            $motDePasse = $_POST['mot-de-passe'];
            $adresseMail = $_POST['email'];
            $nomPhotoDeProfil = $_FILES['nom-photo-de-profil'];

            if (strlen($login) < 4 || strlen($login) > 20) {
                MessageFlash::ajouter("error", "Le login doit être compris entre 4 et 20 caractères!");
                return ControleurUtilisateur::rediriger("inscription", (array)"afficherFormulaireCreation");
            }
            if (!preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$#", $motDePasse)) {
                MessageFlash::ajouter("error", "Mot de passe invalide!");
                return ControleurUtilisateur::rediriger("inscription", (array)"afficherFormulaireCreation");
            }
            if (!filter_var($adresseMail, FILTER_VALIDATE_EMAIL)) {
                MessageFlash::ajouter("error", "L'adresse mail est incorrecte!");
                return ControleurUtilisateur::rediriger("inscription", (array)"afficherFormulaireCreation");
            }

            $utilisateurRepository = new UtilisateurRepository();
            $utilisateur = $utilisateurRepository->recupererParLogin($login);
            if ($utilisateur != null) {
                MessageFlash::ajouter("error", "Ce login est déjà pris!");
                return ControleurUtilisateur::rediriger("inscription", (array)"afficherFormulaireCreation");
            }

            $utilisateur = $utilisateurRepository->recupererParEmail($adresseMail);
            if ($utilisateur != null) {
                MessageFlash::ajouter("error", "Un compte est déjà enregistré avec cette adresse mail!");
                return ControleurUtilisateur::rediriger("inscription", (array)"afficherFormulaireCreation");
            }

            $mdpHache = MotDePasse::hacher($motDePasse);

            // Upload des photos de profil
            // Plus d'informations :
            // http://romainlebreton.github.io/R3.01-DeveloppementWeb/assets/tut4-complement.html

            // On récupère l'extension du fichier
            $explosion = explode('.', $nomPhotoDeProfil['name']);
            $fileExtension = end($explosion);
            if (!in_array($fileExtension, ['png', 'jpg', 'jpeg'])) {
                MessageFlash::ajouter("error", "La photo de profil n'est pas au bon format!");
                return ControleurUtilisateur::rediriger("inscription", (array)"afficherFormulaireCreation");
            }
            // La photo de profil sera enregistrée avec un nom de fichier aléatoire
            $pictureName = uniqid() . '.' . $fileExtension;
            $from = $nomPhotoDeProfil['tmp_name'];
            $to = __DIR__ . "/../../ressources/img/utilisateurs/$pictureName";
            move_uploaded_file($from, $to);

            $utilisateur = Utilisateur::create($login, $mdpHache, $adresseMail, $pictureName);
            $utilisateurRepository->ajouter($utilisateur);

            MessageFlash::ajouter("success", "L'utilisateur a bien été créé !");
            return ControleurUtilisateur::rediriger("publications", (array)"afficherListe");
        } else {
            MessageFlash::ajouter("error", "Login, nom, prenom ou mot de passe manquant.");
            return ControleurUtilisateur::rediriger("inscription", (array)"afficherFormulaireCreation");
        }
    }

    public static function afficherFormulaireConnexion(): Response
    {
        return ControleurUtilisateur::afficherTwig('utilisateur/connexion.html.twig');
    }

    public static function connecter(): RedirectResponse
    {
        if (!(isset($_POST['login']) && isset($_POST['mot-de-passe']))) {
            MessageFlash::ajouter("error", "Login ou mot de passe manquant.");
            return ControleurUtilisateur::rediriger("connexion", (array)"afficherFormulaireConnexion");
        }
        $utilisateurRepository = new UtilisateurRepository();
        /** @var Utilisateur $utilisateur */
        $utilisateur = $utilisateurRepository->recupererParLogin($_POST["login"]);

        if ($utilisateur == null) {
            MessageFlash::ajouter("error", "Login inconnu.");
            return ControleurUtilisateur::rediriger("connexion", (array)"afficherFormulaireConnexion");
        }

        if (!MotDePasse::verifier($_POST["mot-de-passe"], $utilisateur->getMdpHache())) {
            MessageFlash::ajouter("error", "Mot de passe incorrect.");
            return ControleurUtilisateur::rediriger("connexion", (array)"afficherFormulaireConnexion");
        }

        ConnexionUtilisateur::connecter($utilisateur->getIdUtilisateur());
        MessageFlash::ajouter("success", "Connexion effectuée.");
        return ControleurUtilisateur::rediriger("publications", (array)"afficherListe");
    }

    public static function deconnecter(): RedirectResponse
    {
        if (!ConnexionUtilisateur::estConnecte()) {
            MessageFlash::ajouter("error", "Utilisateur non connecté.");
            return ControleurUtilisateur::rediriger("publications", (array)"afficherListe");
        }
        ConnexionUtilisateur::deconnecter();
        MessageFlash::ajouter("success", "L'utilisateur a bien été déconnecté.");
        return ControleurUtilisateur::rediriger("publications", (array)"afficherListe");
    }
}
