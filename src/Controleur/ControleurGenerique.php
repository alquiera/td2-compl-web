<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\Conteneur;
use TheFeed\Lib\MessageFlash;
use Twig\Environment;

class ControleurGenerique {

    protected static function afficherVue(string $cheminVue, array $parametres = []): Response
    {
        extract($parametres);
        $messagesFlash = MessageFlash::lireTousMessages();
        ob_start();
        require __DIR__ . "/../vue/$cheminVue";
        $corpsReponse = ob_get_clean();
        return new Response($corpsReponse);
    }

    // https://stackoverflow.com/questions/768431/how-do-i-make-a-redirect-in-php
    //renvoyer une nouvelle RedirectResponse vers l’URL absolue qui vient d’être générée.
    protected static function rediriger(string $route, array $parametres = []): RedirectResponse
    {
        // assistant url
        $urlHelper = Conteneur::recupererService("assistantUrl");
        $absoluteUrl = $urlHelper->getAbsoluteUrl($route, $parametres);

        return new RedirectResponse($absoluteUrl);

    }

    public static function afficherErreur($messageErreur = "", $statusCode = 400): ?Response
    {
        $reponse = ControleurGenerique::afficherTwig("erreur.html.twig", [
            "errorMessage" => $messageErreur
        ]);

        $reponse->setStatusCode($statusCode);
        return $reponse;
    }

    protected static function afficherTwig(string $cheminVue, array $parametres = []): Response
    {
        /** @var Environment $twig */
        $twig = Conteneur::recupererService("twig");
        $corpsReponse = $twig->render($cheminVue, $parametres);
        return new Response($corpsReponse);
    }

}